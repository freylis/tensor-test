# coding: utf-8
import argparse

from tensor.rudder import Rudder

# предполагаемая цепочка развития событий
# инициализируется главный "рулевой"
# при инициализации читаем конфиг
# рулевой командует ридеру на чтение
# прочитанный контент отдается анализатору
# анализатор на основе содержимого html-дерева
# решает что важное, а что - нет
# писатель сохраняет файл


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('Url', help='url with article content')

    args = parser.parse_args()

    r = Rudder(args.Url)
    r.run()
    print 'run it'