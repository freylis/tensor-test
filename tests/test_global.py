# coding: utf-8
import responses

from tensor.rudder import Rudder


@responses.activate
def test_local():

    url = 'http://test/result'
    with open('tests/fixtures/custom.html', 'rb') as f:
        fixture_content = f.read()

    expected_result = u"""This my title


some with text and url [http://yandex.ru] from file""".strip()

    responses.add(responses.GET, url, body=fixture_content)

    rudder = Rudder(url)
    rudder.run()

    with open('results/test/result.txt', 'rb') as f:
        parsing_result = f.read()

    assert parsing_result == expected_result
