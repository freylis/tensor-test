# coding: utf-8


class TensorException(Exception):
    message = 'unknown exception'


class UrlContentException(Exception):
    message = 'cant read url'
