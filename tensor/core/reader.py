# coding: utf-8

import requests

from .exceptions.exceptions import UrlContentException


def read_content(url):

    try:
        response = requests.get(url)
    except requests.exceptions.RequestException:
        raise UrlContentException('Cant read url content [%s]' % response.reason)
    if response.status_code != 200:
        raise UrlContentException('Cant read url content [%s]' % response.reason)
    return response.content