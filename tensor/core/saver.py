# coding: utf-8
import re
import os

protocol_pattern = re.compile(r'\w+://')


class Saver(object):

    def __init__(self, analyser):
        self._analyser = analyser

    def save(self, url):
        """
        сохранить по новому пути
        :param url:
        :return:
        """

        relative_path, filename = self._get_relative_path(url)
        # TODO возможность задавать os.getcwd из конфига
        full_path = os.path.join(
            os.getcwd(),
            'results',
            relative_path
        )

        # create directory if does not exist
        if not os.path.exists(full_path):
            os.makedirs(full_path)

        full_filename = os.path.join(full_path, filename)
        with open(full_filename, 'wb') as f:
            f.write(self._get_body().encode('utf-8'))
        return None

    def _get_body(self):

        result = ['%s%s' % (self._analyser.get_title(), os.linesep)]
        result.extend([element.get_clear_text() for element in self._analyser.elements])
        return ('%s%s' % (os.linesep, os.linesep)).join(result)

    def _get_relative_path(self, url):
        """
        возвращает относительный путь для сохранения файла
        :return: relative path
        """

        # вырезать протокол из url`a
        url_without_protocol = protocol_pattern.sub(lambda match: '', url)

        # получить список названий директорий и имя файла
        path_parts = url_without_protocol.split('/')

        # удалить пустые значения
        path_parts = filter(lambda dirname: bool(dirname.strip()), path_parts)

        # filename
        directories = path_parts[:-1]
        filename = '%s.txt' % path_parts[-1]

        return os.path.join(*directories), filename
