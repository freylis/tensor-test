# coding: utf-8

from bs4 import BeautifulSoup

from tensor.conf import config
from .element import Element
from .exceptions import WrongElementError


class Analyser(object):

    def __init__(self, content):
        self.content = content
        self.tree = BeautifulSoup(self.content)
        self.elements = []

    def analyse(self):
        """
        проанализировать html на предмет нахождения текста в тегах, указанных в config.ini файле
        На основе бально системы принять решение о том, кто-же по нашему мнению
        содержит полезный контент, а кто - нет
        :return: list of checked elements
        """
        all_tags = config.get('content_tags')
        all_elements = []
        for tag_name in all_tags:

            # возвращает структуру вида:
            # {
            #     <level_5>: [Element1, Element2],
            #     <level_7>: [Element3, Element4],
            # }
            tag_elements = self._search_with_tag(tag_name)

            # подсчитывает кол-во баллов для структуры <tag_elements>
            # возвращает результат в виде:
            # <scores_count>, [(<level_scores>, [Elements]), (<level_scores>, [Elements])]
            tag_scores, elements = self._handle_tag(tag_elements)

            # добавляет результат обработки всех тегов по имени
            all_elements.append((tag_scores, elements))

        # какой элемент набрал больше баллов - тот и выиграл
        unsorted_groups = sorted(all_elements, key=lambda tag_results: tag_results[0], reverse=True)[0][1]
        sorted_groups = sorted(unsorted_groups, key=lambda group: group[0], reverse=True)

        # теперь из этого тега нужно отобрать те элементы, которые "достойны" быть в победителях
        # отбор происходит по следующей схеме:
        # ищется наиболее успешная группа элементов
        # далее идем по всем группам. Если очков у группы >= <min_group_score_percents> (из настроек) - группа проходит целиком
        best_group_scores = sorted_groups[0][0]

        min_group_score_percents = config.get('min_group_score_percents')
        min_scores = round(best_group_scores/min_group_score_percents, 2)

        for group_scores, elements in sorted_groups:
            if group_scores >= min_scores:
                self.elements += elements
            else:
                break

    def get_title(self):

        title_tags = config.get('title_tags')

        for tag_name in title_tags:
            tag = self.tree.find(tag_name)
            if tag is not None:
                try:
                    element = Element(tag)
                except WrongElementError:
                    pass
                else:
                    return element.get_clear_text()
        return u'Заголовок не найден'

    def _search_with_tag(self, tagname):
        """
        найти все теги с нужным именем.
        Возвращает список элементов, которые уже проверенны на содержание в них текста
        :param tagname:
        :return: elements list
        """
        tags = self.tree.findAll(tagname)
        elements = {}
        for tag in tags:
            try:
                element_level, element = self._handle_element(tag)
            except WrongElementError:
                pass
            else:
                if element_level in elements.keys():
                    elements[element_level].append(element)
                else:
                    elements[element_level] = [element]
        return elements.items()

    def _handle_element(self, tag):
        """
        Попытаться сделать из тега элемент для анализа
        Если элемент, по нашму мнению, не содержит ничего полезного - возникает исключение WrongElementError
        :param tag:
        :return: уровень тега, элемент
        """
        element = Element(tag)
        element_level = element.get_level()
        return element_level, element

    def _handle_tag(self, tagname_items):
        result = []
        scores = 0
        for tag_level, element_list in tagname_items:
            level_scores = self._tag_level_scores(element_list)
            scores += level_scores
            result.append((level_scores, element_list))
        return scores, result

    def _tag_level_scores(self, elements):
        """
        посчитать кол-во баллов за список одноуровневых элементов
        :param elements:
        :return: int scores
        """
        scores_per_item = config.get('content_level_score')
        scores = len(elements)*scores_per_item
        for element in elements:
            scores += element.get_scores()
        return scores