# coding: utf-8

from tensor.core.exceptions.exceptions import TensorException


class WrongElementError(TensorException):
    message = 'This element is unnecessary'


class WrongContentPart(TensorException):
    message = 'Incorrect tag in paragraph'
