# coding: utf-8
import os
import re

from bs4 import BeautifulSoup
from bs4.element import NavigableString

from tensor.conf import config
from .exceptions import WrongElementError
from .exceptions import WrongContentPart


AVAILABLE_CONTENT_TAGS = [
    'a', 'b', 'strong', 'span',
    'div', 'br', 'i', 'ul', 'li',
    # etc
]


class Element(object):
    """
    вся работа, связанная конкретно с однм тегом, например:
    <p>Это текст параграфа</p>
    сосредоточена здесь
    """

    MAX_LINE_LENGTH = config.get('max_line_length')

    def __init__(self, tag):
        self._tag = tag
        self._text = self._get_text()

    def get_scores(self):
        """
        подсчитывает виртуальное кол-во очков, которое по правилам этой системы можно дать тегу
        :return: int scores count
        """
        return len(self._text)*config.get('content_char_score')

    def get_clear_text(self):
        """
        возвращает текст, содержащийся внутри тега, предварительно обработанный
        :return: basestring text
        """
        try:
            return self._separate_line(self._text)
        except RuntimeError:
            return u'<Найден контент, который я не могу распарсить>'

    def get_level(self):
        """
        возвращает уровень тега, например:
        <html>
            <body>
                <div>
                    <p>ЭТОТ ТЕГ</p>
                </div>
            </body>
        </html>
        вернет 4
        :return:
        """
        return len(list(self._tag.parentGenerator()))

    def _get_text(self):
        """
        Ищет текст внутри тега. Если мы решаем, что текст фуфло - рейзим исключение WrongElementError
        Если текст найден - он отформатирован
        :return: basestring text
        """
        content_parts = self._tag.contents
        result = []
        for content_part in content_parts:
            try:
                result.append(self._get_part_text(content_part))
            except WrongContentPart:
                pass
        result_string = ''.join(result)

        if len(result_string) < config.get('min_string_length'):
            raise WrongElementError('Incorrect tag "%s"' % self._tag)
        return result_string

    def _get_part_text(self, part):
        """
        попробовать обернуть в soup. Если обернется - проверить, можем ли мы показывать содержимое этого тега
        :param part:
        :return: string text
        """

        pseudo_html = '<mytag>%s</mytag>' % part
        soup = BeautifulSoup(pseudo_html)
        my_tag = soup.find('mytag')
        part_tag = my_tag.children.next()
        part_tag_name = part_tag.name
        if (part_tag_name is not None) and (part_tag_name not in AVAILABLE_CONTENT_TAGS):
            raise WrongContentPart('Incorrect tag <%s> in paragraph "%s"' % (part_tag_name, part))

        # если это текст - так и вернуть текст
        if isinstance(part_tag, NavigableString):
            if not isinstance(part_tag, basestring):
                return str(part_tag).strip()
            else:
                return part_tag.strip()

        text = part_tag.text

        # отдельно обработаем ссылку
        if part_tag_name == 'a':
            url = part_tag.attrs.get('href')
            return ' %s ' % ('%s [%s]' % (text, url)).strip()

        # отдельно обработаем <br>
        if part_tag_name == 'br':
            return '%s' % os.linesep

        # остальные теги по стандартной схеме - только текст
        return text.strip()

    @classmethod
    def _separate_line(cls, substring, separated_text=None):
        """
        взять первые 81 символ и получить последний пробел в этой строке
        :param substring:
        :param separated_text:
        :return:
        """

        # это уже обработанный текст
        separated_text = separated_text or ''

        # взять подстроку с длиной, заведомо больше максимальной
        current_part = substring[:cls.MAX_LINE_LENGTH]

        # если подстроку вообще не нужно разделять
        if len(current_part) < cls.MAX_LINE_LENGTH:

            # нечего больше разделять в этой строке
            separated_text += current_part.strip()

            return separated_text

        # то всю строку разделить по пробелам
        string_parts = current_part.split(' ')

        # все слова до последнего пробела
        current_substring = ' '.join(string_parts[:-1])

        separated_text += '%s%s' % (current_substring.strip(), os.linesep)

        # следущая часть начинается с подстроки, непоместившейся в текущую подстроки
        next_substring = '%s%s' % (string_parts[-1], substring[cls.MAX_LINE_LENGTH:])
        return cls._separate_line(next_substring, separated_text)