# coding: utf-8
import ConfigParser

from .exceptions import ConfigException


AVAILABLE_TYPES = {
    list: lambda value: value.split(','),
    int: int,
    float: lambda value: round(float(value), 2),
    basestring: basestring,
}


class Config(object):
    """
    read settings from ini file and from settings
    """
    DEFAULT_SECTION = 'default'
    CONTENT_SECTION = 'content'
    TITLE_SECTION = 'title'

    OPTION_SECTIONS = {
        'content_tags': {
            'section': CONTENT_SECTION,
            'type': list,
        },
        'content_level_score': {
            'section': CONTENT_SECTION,
            'type': float,
        },
        'min_group_score_percents': {
            'section': CONTENT_SECTION,
            'type': float,
        },
        'min_string_length': {
            'section': CONTENT_SECTION,
            'type': int,
        },
        'content_char_score': {
            'section': CONTENT_SECTION,
            'type': float,
        },
        'max_line_length': {
            'section': CONTENT_SECTION,
            'type': int,
        },
        'title_tags': {
            'section': TITLE_SECTION,
            'type': list,
        },
        'title_level_score': {
            'section': TITLE_SECTION,
            'type': float,
        },
    }

    def __init__(self):
        self._config = self._get_config()

    def get(self, option):
        """
        get option value from config file
        """
        if not isinstance(option, basestring):
            raise ConfigException('unknown config option `%s`' % option)

        option_settings = self.OPTION_SECTIONS.get(option)
        if option_settings is None:
            raise ConfigException('unknown config option `%s`' % option)
        option_section = option_settings.get('section') or self.DEFAULT_SECTION
        if option_section is None:
            raise ConfigException('unknown config option `%s`' % option)

        value = self._config.get(option_section, option)
        value_type = option_settings.get('type') or basestring
        value_type_function = AVAILABLE_TYPES.get(value_type) or basestring
        try:
            return value_type_function(value)
        except (TypeError, ValueError):
            raise ConfigException('invalid config value %s=%s' % (option, value))

    @classmethod
    def _get_config(cls):
        """
        прочитать все пути, где могут быть конфиги
        """
        config = ConfigParser.ConfigParser()
        config.read(['config.ini'])
        return config


config = Config()