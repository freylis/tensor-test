# coding: utf-8

from tensor.core.exceptions.exceptions import TensorException


class ConfigException(TensorException):
    message = 'unknown option'