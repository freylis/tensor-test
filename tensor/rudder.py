# coding: utf-8
from .core.reader import read_content
from .analyser.analyser import Analyser
from .core.saver import Saver


class Rudder(object):

    def __init__(self, url):
        self.url = url
        self.content = read_content(url)

    def run(self):
        self.analyser = Analyser(self.content)
        self.analyser.analyse()
        saver = Saver(self.analyser)
        saver.save(self.url)
        # TODO writer must be save result


